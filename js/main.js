
let slider = document.getElementsByClassName('slider');
console.log(slider);
let imgs = [
    'images/1.jpeg',
    'images/2.jpeg',
    'images/3.jpeg',
    'images/4.jpeg',
    'images/5.jpeg',
];
let img = document.createElement('img');
img.setAttribute('class', 'image');
img.setAttribute('src', 'images/1.jpeg');
slider[0].appendChild(img);

let sType;

// next button
let next = document.createElement('a');
let imgN = document.createElement('img');
imgN.setAttribute('src', 'images/next.png');
imgN.setAttribute('class', 'imgN');
next.setAttribute('class', 'next');
next.appendChild(imgN);
slider[0].appendChild(next);

next.onclick = () => {
    let currentimg = img.getAttribute('src');
    console.log(currentimg);
    let currentindex = imgs.indexOf(currentimg);
    console.log(currentindex);
    if (currentindex === imgs.length - 1) {
        img.setAttribute('src', imgs[0]);
        console.log(imgs[0]);
    } else {
        img.setAttribute('src', imgs[currentindex + 1]);
        console.log(imgs[currentindex + 1]);
    }
};

// prev button

let prev = document.createElement('a');
let imgp = document.createElement('img');
imgp.setAttribute('src', 'images/left.png');
imgp.setAttribute('class', 'imgN');
prev.setAttribute('class', 'prev');
prev.appendChild(imgp);
slider[0].insertBefore(prev, slider[0].firstChild);

prev.onclick = () => {
    let currentimg = img.getAttribute('src');
    console.log(currentimg);
    let currentindex = imgs.indexOf(currentimg);
    console.log(currentindex);
    if (currentindex === 0) {
        img.setAttribute('src', imgs[imgs.length - 1]);
        console.log(imgs[imgs.length - 1]);
    } else {
        img.setAttribute('src', imgs[currentindex - 1]);
        console.log(imgs[currentindex - 1]);
    }
};

let buttons = document.createElement('div');
buttons.setAttribute('class', 'buttons');
slider[0].appendChild(buttons);

// autoplay button
let auto = document.createElement('button');
auto.textContent = 'Autoplay';
buttons.appendChild(auto);

auto.onclick = () => {
    if (sType) {
        clearInterval(sType);
    }
    sType = setInterval(autoplay, 2000);
};

function autoplay() {
    next.click();
}

// random button
let random = document.createElement('button');
random.textContent = 'Random';
buttons.appendChild(random);
random.onclick = () => {
    if (sType) {
        clearInterval(sType);
    }
    sType = setInterval(randomFun, 2000);
};

function randomFun() {
    let i = imgs[Math.floor(Math.random() * imgs.length)];
    console.log(imgs.indexOf(i));
    img.setAttribute('src', i);
    console.log('randomFun');
}
