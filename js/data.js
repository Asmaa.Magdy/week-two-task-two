/*async function getData() {
    const response = await fetch('https://jsonplaceholder.typicode.com/posts');
    const data = await response.json();
    let titles = data.map((el) => el.title);
    console.log(titles);
    console.log('fetch');
    let bodys = data.map((el) => el.body);
    console.log(bodys);
    console.log('fetch');
}

getData();

*/
let slider = document.getElementsByClassName('slider');

async function getData() {
    const response = await axios.get(
        'https://jsonplaceholder.typicode.com/posts'
    );
    console.log(response);
    let titles = response.data.map((el) => el.title);
    let bodys = response.data.map((el) => el.body);
    let title = document.querySelector('.title');
    let body = document.querySelector('.body');
    title.innerText += response.data[0].title;
    body.innerText += response.data[0].body;

    let sType;

    // next button
    let next = document.createElement('a');
    let imgN = document.createElement('img');
    imgN.setAttribute('src', 'images/next.png');
    imgN.setAttribute('class', 'imgN');
    next.setAttribute('class', 'next');
    next.appendChild(imgN);
    slider[0].appendChild(next);

    next.onclick = () => {
        let currentData = title.innerText;
        let currentindex = titles.indexOf(currentData);

        if (currentindex === titles.length - 1) {
            title.innerText = titles[0];
            body.innerText = bodys[0];
        } else {
            console.log(currentindex);
            title.innerText = titles[currentindex + 1];
            body.innerText = bodys[currentindex + 1];
        }
    };

    // prev button

    let prev = document.createElement('a');
    let imgp = document.createElement('img');
    imgp.setAttribute('src', 'images/left.png');
    imgp.setAttribute('class', 'imgN');
    prev.setAttribute('class', 'prev');
    prev.appendChild(imgp);
    slider[0].insertBefore(prev, slider[0].firstChild);

    prev.onclick = () => {
        let currentData = title.innerText;

        console.log(currentData);
        let currentindex = titles.indexOf(currentData);

        if (currentindex === 0) {
            title.innerText = titles[titles.length - 1];
            body.innerText = bodys[titles.length - 1];
        } else {
            console.log(currentindex);
            title.innerText = titles[currentindex - 1];
            body.innerText = bodys[currentindex - 1];
        }
    };
    let buttons = document.createElement('div');
    buttons.setAttribute('class', 'buttons');
    slider[0].appendChild(buttons);

    // autoplay button
    let auto = document.createElement('button');
    auto.textContent = 'Autoplay';
    buttons.appendChild(auto);

    auto.onclick = () => {
        if (sType) {
            clearInterval(sType);
        }
        sType = setInterval(autoplay, 2000);
    };

    function autoplay() {
        next.click();
    }
    // random button
    let random = document.createElement('button');
    random.textContent = 'Random';
    buttons.appendChild(random);
    random.onclick = () => {
        if (sType) {
            clearInterval(sType);
        }
        sType = setInterval(randomFun, 2000);
    };

    function randomFun() {
        let i = titles[Math.floor(Math.random() * titles.length)];
        console.log(titles.indexOf(i));
        title.innerText = i;
        body.innerText = i;

        console.log('randomFun');
    }
}
getData();

/*
let titles;
let bodys;
async function getData() {
    try {
        const response = await axios.get(
            'https://jsonplaceholder.typicode.com/posts'
        );
        titles = response.data.map((res) => res.title);
        bodys = response.data.map((res) => res.body);
        let title = document.createElement('h2');
     let body = document.createElement('p');
let dataDiv = document.createElement('div');
title.innerText(titles[0])
dataDiv.appendChild(title);
dataDiv.appendChild(body);
document.body.appendChild(dataDiv);

    } catch (error) {
        return error;
    }
}

let g = getData();

console.log(g);

export { getData, titles, bodys };
*/
